<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['summary','date'];

    public function candidates(){
        return $this->hasMany('App\Candidate');

}
    public function users(){
        return $this->belongsTo('App\User');
}

}