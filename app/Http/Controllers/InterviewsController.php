<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.index', compact('interviews','candidates','users'));

    }
    public function changeCandidate($cid, $iid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->interview_id = $iid;
        $candidate->save(); 
        return redirect('interviews');
    }

    public function changeUser($iid, $uid){
        $interview = User::findOrFail($uid);
        $interview->user_id = $uid;
        $interview->save(); 
        return redirect('interviews');
    }
    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();
        $candidates = Candidate::all();        
        return view('interviews.index', compact('candidates','users', 'interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $candidates = Candidate::all();
        $interviews = interview::all();

        return view('interviews.create', compact('interviews','candidates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $interview = $interview->create($request->all());
        return redirect('interview');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
