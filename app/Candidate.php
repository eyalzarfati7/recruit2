<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status(){
        return $this->belongsTo('App\status');
    }
    public function Department(){
        return $this->belongsTo('App\department');
    }
    public function Interview(){
        return $this->belongsTo('App\interview');
    }
}

