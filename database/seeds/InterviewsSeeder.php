<?php

use Illuminate\Database\Seeder;
use carbon\carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'summary' => 'This is the interview summary',
                'Date' => carbon::now(),
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ],
            [ 
                'summary' => 'This is the interview summary',
                'Date' => carbon::now(),
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ],
            ]);
        }
    
    }