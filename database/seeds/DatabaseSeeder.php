<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* allows you to break up your database seeding into
         multiple files so that no single seeder class becomes overwhelmingly large.*/
        $this->call(CandidatesSeeder::class);
    }
}
