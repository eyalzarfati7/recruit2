<?php

use Illuminate\Database\Seeder;
use carbon\carbon;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        [
            'name' => 'before interview',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ],
        [ 
            'name' => 'not fit',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ],
        [ 
            'name' => 'sent to manager',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ],
        [ 
            'name' => 'not fit professionally',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ],
        [ 
            'name' => 'accepted to work',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ]
        ]);
    }

}



    

