@extends('layouts.app')

@section('title', 'create candidate')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf
        <div class="form-group">
            <label for="summary">Interview Summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div>
        <div class="form-group">
            <label for="date">Interview date</label>
            <input type = "text" class="form-control" name = "email">
        </div>
        <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Choose candidate to interview
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach($candidates as $candidate)
                <a class="dropdown-item" href="#">{{$candidate->name}}</a>
            @endforeach
        </div>
        
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>
        
        </form>

    @endsection        
   