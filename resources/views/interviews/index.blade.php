@extends('layouts.app')

@section('title', 'interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Summary</th><th>Interviewer</th><th>Candidate</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>       
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->summary}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($interview->user_id))
                          {{$interview->user->name}}  
                        @else
                          Assign Interviewer
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                      <a class="dropdown-item" href="{{route('interview.changecandidate',[$interview->id,$user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                </div>                
            </td>
            <td>
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Assign Candidate
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($candidates as $candidate)
                      <a class="dropdown-item" href="{{route('interview.changecandidate',[$candidate->id,$interview->id])}}">{{$candidate->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td>
            
          


    @endforeach
</table>
@endsection
